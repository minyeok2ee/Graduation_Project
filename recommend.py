import json
from urllib import parse
import random

emo_gen = {
    'neutral': ['kpop_balad', 'kpop_dance', 'kpop_r&b/soul', 'kpop_trot', 'kpop_folk/blues',
                'pop_pop', 'pop_electronica', 'pop_r&b/soul', 'pop_folk/blues'],
    'surprised': ['kpop_balad', 'kpop_r&b/soul', 'kpop_indi', 'kpop_folk/blues',
                  'pop_pop', 'pop_r&b/soul', 'pop_folk/blues'],
    'happy': ['kpop_dance', 'kpop_rap/hiphop', 'kpop_trot', 'kpop_folk/blues',
              'pop_pop', 'pop_rock/metal', 'pop_r&b/soul', 'pop_folk/blues'],
    'sad': ['kpop_balad', 'kpop_r&b/soul', 'kpop_indi', 'kpop_folk/blues',
            'pop_pop', 'pop_r&b/soul', 'pop_folk/blues'],
    'angry': ['kpop_rap/hiphop', 'kpop_rock/metal',
              'pop_rock/metal', 'pop_electronica', 'pop_rap/hiphop'],
    'fearful': ['kpop_balad', 'kpop_r&b/soul', 'kpop_indi', 'kpop_folk/blues',
                'pop_pop', 'pop_r&b/soul', 'pop_folk/blues'],
    'disgusted': ['kpop_rap/hiphop', 'kpop_rock/metal',
                'pop_rock/metal', 'pop_electronica', 'pop_rap/hiphop']
}

recommend_data = {}


def load_data():
    with open('./analysis_data/music_list_steady_seller.json', 'r+', encoding='utf-8') as s_f:
        steady = json.loads(s_f.read())
    with open('./analysis_data/music_list_latest_song.json', 'r+',  encoding='utf-8') as l_f:
        latest = json.loads(l_f.read())
    return steady, latest


def recommend(emotion):
    steady, latest = load_data()

    url = 'https://www.youtube.com/results?search_query='
    random_genres = random.sample(emo_gen[emotion], 3)

    recommend_data.clear()
    for genre in random_genres:
        print('추천된 장르 : ', genre)
        song_list_s = random.sample(steady[genre], 10)
        song_list_l = random.sample(latest[genre], 10)

        for index in range(0, 10):
            singer = song_list_s[index][0]
            song = song_list_s[index][1]
            url_result = url + singer + '+' + song
            url_result = parse.urlparse(url_result)
            query = parse.parse_qs(url_result.query)
            url_result = parse.urlencode(query, doseq=True)
            song_list_s[index].append('https://www.youtube.com/results?' + url_result)
        recommend_data[genre+'  :  스테디 셀러'] = song_list_s
        for index in range(0, 10):
            singer = song_list_l[index][0]
            song = song_list_l[index][1]
            url_result = url + singer + '+' + song
            url_result = parse.urlparse(url_result)
            query = parse.parse_qs(url_result.query)
            url_result = parse.urlencode(query, doseq=True)
            song_list_l[index].append('https://www.youtube.com/results?' + url_result)
        recommend_data[genre+'  :  최신곡'] = song_list_l
    return recommend_data


def print_list_html(data):
    keys = data.keys()
    details = ''
    for key in keys:
        details += '<details><summary>'+str(key)+'</summary>'
        musics = data[key]
        table = '<table class="table_5">' \
                '<tr class="even">' \
                '<td>가수</td>' \
                '<td>노래</td>' \
                '<td>앨범</td>' \
                '<td>링크</td>' \
                '</tr>'
        for music in musics:
            singer = music[0]
            song = music[1]
            album = music[2]
            url = music[3]
            song_info = '<tr><td>' + singer +\
                        '</td><td>' + song +\
                        '</td><td>' + album +\
                        '</td><td>' \
                        '<a href="' + url + '" target="_blank">음악 듣기</a></td></tr>'
            table += song_info
        table += '</table>'
        details += table
        details += '</details>'
    return details

