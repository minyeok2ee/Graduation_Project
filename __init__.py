from flask import Flask, render_template, Response, jsonify, request, redirect
# from face_analysis import VideoCamera, init_emotion, img_analysis
import voice_analysis
from voice_analysis import init_voice, init_emotion
import timeutil
import recommend
from werkzeug.utils import secure_filename
from pydub import AudioSegment


app = Flask(__name__)

emotion_str = ""


@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')


def gen(camera):
    try:
        while True:
            frame = camera.get_frame()
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')
    except:
        pass


@app.route('/video_feed')
def video_feed():
    return Response(gen(VideoCamera()),
                    mimetype='multipart/x-mixed-replace; boundary=frame')


@app.route('/face_execute')
def face_execute():
    global emotion_str
    emotion_str = VideoCamera().face_result()
    return jsonify(emotion_str)


def img_execute():
    global emotion_str
    emotion_str = img_analysis()
    return jsonify(emotion_str)


@app.route('/face_emotion')
def face_emotion():
    return render_template('face_emotion_analysis.html')


@app.route('/voice_execute')
def voice_execute():
    speech = voice_analysis.get_speech()
    return jsonify(speech)


@app.route('/voice_result')
def voice_result():
    global emotion_str
    file_name = "./analysis_data/test.wav"
    emotion_str = voice_analysis.voice_analysis(file_name)
    return jsonify(emotion_str)


@app.route('/voice_emotion')
def voice_emotion():
    return render_template('voice_emotion_analysis.html')


@app.route('/recommend_result')
def recommend_result():
    global emotion_str
    if emotion_str == 'fail':
        return jsonify(None)
    elif emotion_str == '분석에 실패 했습니다. 초기화 후 다시 시작해주세요.':
        return jsonify(None)
    else:
        print(emotion_str)
        music_data = recommend.recommend(emotion_str)
        music_list = recommend.print_list_html(music_data)
        return jsonify(music_list)


@app.route('/analysis_result')
def analysis_result():
    result = request.args.get('r')

    timestamp = str(timeutil.get_epochtime_ms())

    if result == 'voice_result':
        if emotion_str != 'fail':
            e_src = ["static/img/", emotion_str, ".png?", timestamp]
            emotion_src = ''.join(e_src)

            r_src = ["static/img/result.jpg?", timestamp]
            result_src = ''.join(r_src)

            return render_template('analysis_result.html', emotion_src=emotion_src,
                                   result_src=result_src, emotion_str=emotion_str)
        else:
            e_src = ["static/img/mute.png?", timestamp]
            emotion_src = ''.join(e_src)

            r_src = ["static/img/error.png?", timestamp]
            result_src = ''.join(r_src)

            message = "분석에 실패했습니다. 다시 시도해주세요."
            return render_template('analysis_result.html', emotion_src=emotion_src,
                                   result_src=result_src, emotion_str=message)

    elif result == 'face_result':
        if emotion_str != 'fail':
            e_src = ["static/img/face.jpg?", timestamp]
            emotion_src = ''.join(e_src)

            r_src = ["static/img/result.jpg?", timestamp]
            result_src = ''.join(r_src)

            return render_template('analysis_result.html', emotion_src=emotion_src,
                                   result_src=result_src, emotion_str=emotion_str)
        else:
            e_src = ["static/img/test.jpg?", timestamp]
            emotion_src = ''.join(e_src)

            r_src = ["static/img/error.png?", timestamp]
            result_src = ''.join(r_src)

            message = "분석에 실패했습니다. 다시 시도해주세요."

            return render_template('analysis_result.html', emotion_src=emotion_src,
                                   result_src=result_src, emotion_str=message)


@app.route('/test')
def test():
    return render_template('test.html')


@app.route('/testUp', methods=['GET', 'POST'])
def test_up():
    if request.method == 'POST':
        f = request.files['camera']
        f.save('./static/img/' + secure_filename('test.jpg'))
        global emotion_str
        emotion_str = img_analysis()
        return redirect('http://192.168.184.15:8081/analysis_result?r=face_result')


@app.route('/test_v')
def test_v():
    return render_template('test_v.html')


@app.route('/testvUp', methods=['GET', 'POST'])
def testvup():
    if request.method == 'POST':
        f = request.files['audio']
        f.save('./analysis_data/' + secure_filename("raw.amr"))
        sound = AudioSegment.from_file("./analysis_data/raw.amr", "amr")
        sound.export("./analysis_data/test.wav", format="wav")

        global emotion_str
        file_name = "./analysis_data/test.wav"
        #emotion_str = voice_analysis.audio_file_analysis(file_name)
        emotion_str = voice_analysis.voice_analysis(file_name)
        return redirect('http://192.168.184.15:8081/analysis_result?r=voice_result')


if __name__ == '__main__':
    init_emotion()
    init_voice()
    app.run(host='127.0.0.1', port='8081', debug=True)
