# import pyaudio
import speech_recognition as sr
import librosa
import numpy as np
import cv2
import tensorflow as tf
#from keras.models import load_model


def init_emotion():
    global loaded_model
    global session
    session = tf.Session(graph=tf.Graph())
    with session.graph.as_default():
        tf.keras.backend.set_session(session)
        loaded_model = tf.keras.models.load_model("./model/best.model")


def init_voice():
    global test_model
    global session
    session = tf.Session(graph=tf.Graph())
    with session.graph.as_default():
        tf.keras.backend.set_session(session)
        test_model = tf.keras.models.load_model("./model/model6.h5")
        test_model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])


def recognition(audio):
    r = sr.Recognizer()
    result = r.recognize_google(audio)
    return result


def get_speech():
    r = sr.Recognizer()

    with sr.Microphone(sample_rate=48000) as source:
        print("말씀 하세요, 듣고 있습니다.")
        audio = r.listen(source, phrase_time_limit=3)
        input = ""
        try:
            input = r.recognize_google(audio, language='ko-KR')
            print(input)
        except:
            print("분석에 실패 했습니다. 초기화 후 다시 시작해주세요.")
            return "분석에 실패 했습니다. 초기화 후 다시 시작해주세요."

    file_name = "./analysis_data/test.wav"
    with open(file_name, "wb") as f:
        f.write(audio.get_wav_data())

    return input


def process_input_data(file):
    X, sample_rate = librosa.load(file)
    stft = np.abs(librosa.stft(X))
    mfccs = np.mean(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=40).T,axis=0)
    chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T,axis=0)
    mel = np.mean(librosa.feature.melspectrogram(X, sr=sample_rate).T,axis=0)
    contrast = np.mean(librosa.feature.spectral_contrast(S=stft, sr=sample_rate).T,axis=0)
    tonnetz = np.mean(librosa.feature.tonnetz(y=librosa.effects.harmonic(X), sr=sample_rate).T, axis=0)
    
    features = np.empty((0,193))
    ext_features = np.hstack([mfccs,chroma,mel,contrast,tonnetz],)
    features = np.vstack([features, ext_features])
    return np.array(features)


def voice_analysis(file_name):
    emotions = ['neutral', 'surprised', 'happy', 'sad', 'angry', 'fearful', 'disgusted']
    user_input = process_input_data(file_name)
    user_input = user_input.astype('float32')
    user_input /= 255.0

    with session.graph.as_default():
        tf.keras.backend.set_session(session)
        test_model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
        predict = test_model.predict(user_input)
        # predict = test_model.predict_proba(user_input)
        predict = predict[0]
        print(predict)

        result = np.argmax(predict)
        print(result)
        result = emotions[int(result)]
        print(result)

    font = cv2.FONT_HERSHEY_SIMPLEX
    canvas = np.zeros((250, 300, 3), dtype="uint8") + 255

    for (i, (emotion, prob)) in enumerate(zip(emotions, predict)):
        text = '{} : {:.2f}%'.format(emotion, prob * 100)
        w = int(prob * 300)
        cv2.rectangle(canvas, (7, (i * 35) + 5), (w, (i * 35) + 35), (250, 145, 49), -1)
        cv2.putText(canvas, text, (10, (i * 35) + 23), font, 0.65, (0, 0, 0), 1)

#cv2.imshow('Probabilities', canvas)
    cv2.imwrite('./static/img/result.jpg', canvas)
    return result
#cv2.waitKey(0)
#cv2.destroyAllWindows()


#mobile
def audio_file_analysis(file_name):
    r = sr.Recognizer()
    try:
        result = r.recognize_google(file_name)
        print(result)
        result = voice_analysis(file_name)
        return result
    except:
        print("분석에 실패 했습니다. 초기화 후 다시 시작해주세요.")
        return 'fail'
