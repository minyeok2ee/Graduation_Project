function work() {
  for (var i = 0; i < 400000000; i++);
}

function btnEvent(btnName) {
  var btnFrm = document.getElementById(btnName);

  if (btnFrm.value != "분석하기") {
    location.href = '/analysis_result?r=face_result';
  } else {
    $.getJSON('/face_execute', {}, function(txt) {
      if (txt == "fail") {
        alert("분석에 실패 했습니다.\n초기화 후 다시 시작해주세요.");
        btnFrm.value = "분석실패";
        return;
      }
      emotion_str = txt;

      work()

      btnFrm.disabled = false;
      btnFrm.value = "결과확인";
    });
    btnFrm.disabled = true;
    btnFrm.value = "분석중..";
  }
}
