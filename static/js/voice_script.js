function work() {
  for (var i = 0; i < 300000000; i++);
}

function show_txt(txtName, txtName2, btnName) {
  var frm = document.getElementById(txtName);
  var frm2 = document.getElementById(txtName2);
  var btnFrm = document.getElementById(btnName);

  if (btnFrm.value != "분석하기") {
    location.href = '/analysis_result?r=voice_result';
  } else {
    frm.style.display = "block";
    btnFrm.disabled = true;
    btnFrm.value = "분석 중...";

    $.getJSON('/voice_execute', {}, function(data) {
      if (data == "분석에 실패 했습니다. 초기화 후 다시 시작해주세요.") {
        frm2.value = data;
        frm2.style.display = "block";
        btnFrm.value = "분석 실패";
        return;
      }
      var words = data;
      console.log(words);
      frm2.value = words;
      frm2.style.display = "block";

      $.getJSON('/voice_result', {}, function(txt) {
        emotion_str = txt;

        btnFrm.disabled = false;
        btnFrm.value = "결과확인";
      });
    });
  }
}
