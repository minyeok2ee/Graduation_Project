function isMobile() {
  return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
}

function check_face() {
  if (isMobile()) {
    location.href = '/test';
  } else {
    location.href = '/face_emotion';
  }
}

function check_voice() {
  if (isMobile()) {
    location.href = '/test_v';
  } else {
    location.href = '/voice_emotion';
  }
}
