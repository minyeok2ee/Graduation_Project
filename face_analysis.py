# import cv2
# import numpy as np
# import tensorflow as tf
#
# faceDet = cv2.CascadeClassifier("./analysis_data/haarcascade_frontalface_default.xml")
# faceDet_two = cv2.CascadeClassifier("./analysis_data/haarcascade_frontalface_alt2.xml")
# faceDet_three = cv2.CascadeClassifier("./analysis_data/haarcascade_frontalface_alt.xml")
# faceDet_four = cv2.CascadeClassifier("./analysis_data/haarcascade_frontalface_alt_tree.xml")
#
# settings = {
#     'scaleFactor': 1.3,
#     'minNeighbors': 5,
#     'minSize': (50, 50)}
# font = cv2.FONT_HERSHEY_SIMPLEX
#
# emotions = ["angry", "disgusted", "fearful", "happy", "neutral", "sad", "surprised"]
#
#
#
#
# class VideoCamera(object):
#     def __init__(self):
#         self.camera = cv2.VideoCapture(0)
#
#     def __del__(self):
#         self.camera.release()
#         cv2.destroyAllWindows()
#
#     def get_frame(self):
#         ret, img = self.camera.read()
#
#         _, jpeg = cv2.imencode('.jpg', img)
#         return jpeg.tobytes()
#
#     def face_result(self):
#         ret, img = self.camera.read()
#
#         gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
#
#         # Detect face using 4 different classifiers
#         faces_one = faceDet.detectMultiScale(gray, **settings)
#         faces_two = faceDet_two.detectMultiScale(gray, **settings)
#         faces_three = faceDet_three.detectMultiScale(gray, **settings)
#         faces_four = faceDet_four.detectMultiScale(gray, **settings)
#
#         if len(faces_one) == 1:
#             faces = faces_one
#         elif len(faces_two) == 1:
#             faces = faces_two
#         elif len(faces_three) == 1:
#             faces = faces_three
#         elif len(faces_four) == 1:
#             faces = faces_four
#         else:
#             return 'fail'
#
#         canvas = np.zeros((250, 300, 3), dtype="uint8") + 255
#         font = cv2.FONT_HERSHEY_SIMPLEX
#         face_emotion = ""
#         pred = np.zeros(8)
#         face_img = np.zeros(0)
#
#         for (x, y, w, h) in faces:
#             cv2.rectangle(img, (x + 10, y + 10), (x + w - 10, y + h - 10), (245, 135, 66), 5)
#             cv2.rectangle(img, (x + 10, y + 10), (x + w - 10, y + 65), (245, 135, 66), -1)
#             face = gray[y + 5:y + h - 5, x + 20:x + w - 20]
#             face_img = img[y:y + h, x:x + w]
#             face = cv2.resize(face, (64, 64))
#             face = face / 255.0
#
#             with session.graph.as_default():
#                 tf.keras.backend.set_session(session)
#                 pred = loaded_model.predict(np.array([face.reshape((64, 64, 1))]))[0]
#
#             face_emotion = emotions[pred.argmax()]
#             cv2.putText(img, face_emotion, (x + 25, y + 55), font, 1.5, (255, 255, 255), 2, cv2.LINE_AA)
#
#             print(pred)
#
#         for (i, (emotion, prob)) in enumerate(zip(emotions, pred)):
#             text = '{} : {:.2f}%'.format(emotion, prob * 100)
#             w = int(prob * 300)
#             cv2.rectangle(canvas, (7, (i * 35) + 5), (w, (i * 35) + 35), (250, 145, 49), -1)
#             cv2.putText(canvas, text, (10, (i * 35) + 23), font, 0.65, (0, 0, 0), 1)
#
#         try:
#             face_img = cv2.resize(face_img, (512, 512))
#
#             cv2.imwrite('./static/img/face.jpg', face_img)
#             cv2.imwrite('./static/img/result.jpg', canvas)
#
#             return face_emotion
#         except:
#             return 'fail'
#
#
# def img_analysis():
#     img = cv2.imread('./static/img/test.jpg')
#
#     if img is None:
#         return 'fail'
#
#     gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
#
#     # Detect face using 4 different classifiers
#     faces_one = faceDet.detectMultiScale(gray, **settings)
#     faces_two = faceDet_two.detectMultiScale(gray, **settings)
#     faces_three = faceDet_three.detectMultiScale(gray, **settings)
#     faces_four = faceDet_four.detectMultiScale(gray, **settings)
#
#     if len(faces_one) == 1:
#         faces = faces_one
#     elif len(faces_two) == 1:
#         faces = faces_two
#     elif len(faces_three) == 1:
#         faces = faces_three
#     elif len(faces_four) == 1:
#         faces = faces_four
#     else:
#         img = cv2.resize(img, (512, 512))
#         cv2.imwrite('./static/img/test.jpg', img)
#         return 'fail'
#
#     canvas = np.zeros((250, 300, 3), dtype="uint8") + 255
#     font = cv2.FONT_HERSHEY_SIMPLEX
#     face_emotion = ""
#     pred = np.zeros(8)
#     face_img = np.zeros(0)
#
#     for (x, y, w, h) in faces:
#         cv2.rectangle(img, (x + 10, y + 10), (x + w - 10, y + h - 10), (245, 135, 66), 5)
#         cv2.rectangle(img, (x + 10, y + 10), (x + w - 10, y + 65), (245, 135, 66), -1)
#         face = gray[y + 5:y + h - 5, x + 20:x + w - 20]
#         face_img = img[y:y + h, x:x + w]
#         face = cv2.resize(face, (64, 64))
#         face = face / 255.0
#
#         with session.graph.as_default():
#             tf.keras.backend.set_session(session)
#             pred = loaded_model.predict(np.array([face.reshape((64, 64, 1))]))[0]
#
#         face_emotion = emotions[pred.argmax()]
#         cv2.putText(img, face_emotion, (x + 25, y + 55), font, 1.5, (255, 255, 255), 2, cv2.LINE_AA)
#
#         print(pred)
#
#     for (i, (emotion, prob)) in enumerate(zip(emotions, pred)):
#         text = '{} : {:.2f}%'.format(emotion, prob * 100)
#         w = int(prob * 300)
#         cv2.rectangle(canvas, (7, (i * 35) + 5), (w, (i * 35) + 35), (250, 145, 49), -1)
#         cv2.putText(canvas, text, (10, (i * 35) + 23), font, 0.65, (0, 0, 0), 1)
#
#     try:
#         face_img = cv2.resize(face_img, (512, 512))
#
#         cv2.imwrite('./static/img/face.jpg', face_img)
#         cv2.imwrite('./static/img/result.jpg', canvas)
#
#         return face_emotion
#     except:
#         return 'fail'